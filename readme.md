# 单点登陆教程代码

本项目是文章 [跨越万水千山认识你,跨根域的单点登陆设计](https://blog.whyun.com/posts//the-design-of-sso-between-diffrent-domain/) 的配套代码。

## 运行

文章中使用了三个域名，tao.com mao.com sso.com，为了在本地访问这三个域名，首先修改 hosts ，将三个域名映射为 127.0.0.1。然后借助 nginx （或者 apache ）来实现反向代理，以实现访问域名的时候，能够将请求转发到后端的 node 程序，这里仅给出 tao.com 的 nginx 配置示例：

```
upstream backend_tao {
    server 127.0.0.1:3002;
}

server {
    listen       80;
    server_name  tao.com;

    access_log  logs/tao.access.log;
    error_log  logs/tao.error.log;

    location ^~ / {
        proxy_pass  http://backend_tao;
        proxy_redirect off;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;    
        proxy_set_header Host $http_host;
        proxy_connect_timeout   120;
        proxy_send_timeout      120;
        proxy_read_timeout      120;
    }
    
    location = /favicon.ico {
        log_not_found off;
        access_log off;
    }

    #error_page  404              /404.html;

    # redirect server error pages to the static page /50x.html
    #
    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root   html;
    }

}
``` 

其他两个域名和 tao.com 格式一致。

在 `bin` 文件夹中，有三个域名的启动脚本，是 windows 下的批处理格式，这里给出 tao.com 的启动示例：

```bat
set PORT=3002
set DOMAIN_NOW=tao.com
cd ../app
SET DEBUG=*
npm start
```

如果在 linux 下，将 `set` 改为 `export` 即可。